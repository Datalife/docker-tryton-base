#!/bin/sh

docker login registry.gitlab.datalife.es

docker build -q --rm --no-cache -t registry.gitlab.datalife.es/datalife/docker-tryton-base:6.4 .
docker build -q --rm --no-cache -t registry.gitlab.datalife.es/datalife/docker-tryton-base:6.4-office office

docker push registry.gitlab.datalife.es/datalife/docker-tryton-base:6.4-office
docker push registry.gitlab.datalife.es/datalife/docker-tryton-base:6.4