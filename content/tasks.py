# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from invoke import Collection
import tryton_dv

ns = Collection()
ns.add_collection(Collection.from_module(tryton_dv, name='scm'))
